#!/bin/bash

set -u

TEXEC_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
FULL_JSHON_CMD=(jshon -e tag -u -p -e status -u -p -e out -u)
CURRENT_STAGE="${1}"

function add_status {
  local status=$1
  local json=$2
  echo $(echo "${json}" | jshon -s "${status}" -i "status" -j)
}

function tag_output {
  local tag=$1
  local out=$2
  echo $(echo '{}' | jshon -s "${out}" -i "out" -s "${tag}" -i "tag" -j)
}

make_ok=('add_status' 'ok')
make_error=('add_status' 'error')

while read -r -a in; do
  IFS=$'\n' A=($(echo "${in}" | ${FULL_JSHON_CMD[@]}))
  tag=${A[0]}
  st=${A[1]}
  out=${A[2]}

  [ "x${st}" == "xerror" ] && new_out=$out || new_out=$("${TEXEC_ROOT}/stages/${CURRENT_STAGE}.sh" "${tag}" "${out}")
  ([ $? != 0 ] || [ "x${st}" == "xerror" ]) && st_f=(${make_error[@]}) || st_f=(${make_ok[@]})
  printf "%s\n" $(${st_f[@]} $(tag_output "${tag}" "${new_out}"))
done

