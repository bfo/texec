# **texec** #

## Quick summary
Bash tool for watching changes in groups of directories/files (tags) and executing processing pipelines associated with the tags.

## How do I get set up? ###

### Dependencies

`bash`, [`inotify-tools`](https://github.com/rvoicilas/inotify-tools) and [`jshon`](https://github.com/keenerd/jshon).
Please note, that those are only basic dependecies for running `texec` without any plugged pipeline.

### Summary of set up

- Clone the repo
- Define tags in `.texec_modules`, each line takes form: `<tag> <res1> [<res2> ...]`. Paths are always relative to CWD.
- Symlink executables/scripts that you want to be executed in a pipeline to `stages` subdirectory. Name them `<step_num>.sh`, starting from 1. Each of them will be called with two arguments, tag name and output from previous command. If any of the scripts returns nonzero code, the rest of the pipeline will **not** be executed.


## Contribution guidelines ###

Spotted a problem? Idea for an improvement? Let me know!

## Who do I talk to? ###

Author: bfo (bfo.root -> googlemail)