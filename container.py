import sys
import json

from plumbum import local as LocalCmd, LocalPath
from os.path import dirname, realpath
from autocurry import autocurry

stage_script = LocalCmd(LocalPath(dirname(realpath(__file__))) / "stages" / "%d.sh".format(int(sys.argv[1])))

def union(*dicts):
    return dict(i for d in dicts for i in d.items())

@autocurry
def add_status(status, json_d):
    return union(json_d, {'status':status})

def tag_output(tag, command_out):
    return {'out':command_out, 'tag':tag}


make_ok = add_status(True)
make_error = add_status(False)

def next_stage_message(previous_message):
    if not previous_message['status']:
        return make_ok(tag_output(previous_message['tag'], stage_script()))
    else:
        return previous_message

for invocation in sys.stdin:
    try:
        print(json.dumps(next_stage_message(json.loads(invocation))))
    except Exception as e:
        out = "Internal exception:\n%s".format(e.__repr__)
        print(json.dumps({
            'tag':'__unknown__',
            'status': False,
            'out': out
        }))
