#!/bin/bash

set -e
set -u
shopt -s dotglob nullglob

TEXEC_ROOT=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
TEXEC_DOTMODULES="${PWD}/.texec_modules"
NUM_STAGES=$(find "${TEXEC_ROOT}/stages" -maxdepth 1 -type f -name '*.sh' | wc -l)

function setup_tag {
  local num_stages=$1
  local tag=$2
  local nodes=${@:3}
  local WATCH_ACTIONS="modify,create,delete"
  local WATCH_OUTPUT="{\"tag\":\"${tag}\",\"status\":\"ok\",\"out\":\"\"}"
  local WATCH_ARGS=(-q -e ${WATCH_ACTIONS} -m -r -t 0 --format \"${WATCH_OUTPUT}\")

  local CONTAINER_PIPELINE=$(printf "| $(echo ${TEXEC_ROOT} | sed 's/ /\\ /g')/container.sh %d " $(seq 1 $num_stages))

  eval '(' 'inotifywait' ${WATCH_ARGS[@]} ${nodes} ${CONTAINER_PIPELINE} ')' '&'
}

function parse_dotmodules {
  while read line; do
    if [ ! "$line" ]; then break; fi
    local a=($line)
    setup_tag ${NUM_STAGES} ${a[0]} ${a[@]:1}
  done < "${TEXEC_DOTMODULES}";
}

# Guard against missing .texec_modules, which is a fatal error
if [ ! -f "${TEXEC_DOTMODULES}" ]; then echo "Could not find '.texec_modules' file in current directory"; exit 1; fi

# Guard against missing jshon binary
if [ type jshon >/dev/null 2>&1 ]; then echo "Could not find 'jshon' binary. Make sure it's installed and accessible from PATH."; exit 2; fi

# Make sure that every child process spawned by parse_dotmodules is killed on exit
# trap "kill -- $$" SIGINT SIGTERM EXIT

parse_dotmodules
wait
