#!/bin/bash

TEXEC_ROOT=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
pipeline_name="$1"
search_path="${TEXEC_ROOT}/pipelines/${pipeline_name}"

# Quick'n'dirty way to display help
if [ "${pipeline_name}x" == "x" ]; then
    echo "Usage: bootstrap.sh <pipeline_name>"
    exit -2
fi

# Bail out if pipeline directory not found
if [ ! -d ${search_path} ]; then
    echo "${search_path} not found, pipeline configuration not changed"
    exit -1
fi
# Discover all the .sh files in a given directory
file_list="$(find "${search_path}" -maxdepth 1 -name '*.sh' -type f -follow | sort)"

if [ "${file_list}x" == "x" ]; then
    echo "No suitable stage scripts found in ${search_path}"
    exit -3
fi

while read -r stage; do
    ln -s "${stage}" "${TEXEC_ROOT}/stages/$(basename ${stage})"
done <<< "${file_list}"

